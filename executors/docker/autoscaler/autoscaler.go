package autoscaler

import (
	"openbeagle.org/jkridner/gitlab-runner/common"
	"openbeagle.org/jkridner/gitlab-runner/executors/internal/autoscaler"

	_ "openbeagle.org/jkridner/gitlab-runner/executors/docker" // need docker executor registered first
)

func init() {
	common.RegisterExecutorProvider(
		"docker-autoscaler",
		autoscaler.New(
			common.GetExecutorProvider("docker"),
			autoscaler.Config{MapJobImageToVMImage: false},
		),
	)
}
