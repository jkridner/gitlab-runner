//go:build integration && !windows

package autoscaler_test

import "openbeagle.org/jkridner/gitlab-runner/common"

func getImage() string {
	return common.TestAlpineImage
}
