//go:build windows

package parser

import "openbeagle.org/jkridner/gitlab-runner/helpers/path"

func newWindowsPath() Path {
	return path.NewWindowsPath()
}
