package kubernetes

import (
	restclient "k8s.io/client-go/rest"

	"openbeagle.org/jkridner/gitlab-runner/common"
	"openbeagle.org/jkridner/gitlab-runner/executors"
)

// GetKubeClientConfig is used to export the getKubeClientConfig function for integration tests
func GetKubeClientConfig(config *common.KubernetesConfig) (kubeConfig *restclient.Config, err error) {
	return getKubeClientConfig(config, new(overwrites))
}

// NewDefaultExecutorForTest is used to expose the executor to integration tests
func NewDefaultExecutorForTest() common.Executor {
	return &executor{
		AbstractExecutor: executors.AbstractExecutor{
			ExecutorOptions: executorOptions,
		},
	}
}
