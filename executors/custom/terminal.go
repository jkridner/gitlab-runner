//go:build !windows

package custom

import (
	"errors"

	terminalsession "openbeagle.org/jkridner/gitlab-runner/session/terminal"
)

func (e *executor) Connect() (terminalsession.Conn, error) {
	return nil, errors.New("not yet supported")
}
