package main

import (
	"os"
	"path/filepath"

	"github.com/KimMachineGun/automemlimit/memlimit"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"go.uber.org/automaxprocs/maxprocs"

	"openbeagle.org/jkridner/gitlab-runner/common"
	cli_helpers "openbeagle.org/jkridner/gitlab-runner/helpers/cli"
	"openbeagle.org/jkridner/gitlab-runner/log"
	"gitlab.com/gitlab-org/labkit/fips"

	_ "openbeagle.org/jkridner/gitlab-runner/cache/azure"
	_ "openbeagle.org/jkridner/gitlab-runner/cache/gcs"
	_ "openbeagle.org/jkridner/gitlab-runner/cache/s3"
	_ "openbeagle.org/jkridner/gitlab-runner/commands"
	_ "openbeagle.org/jkridner/gitlab-runner/commands/helpers"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/custom"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/docker"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/docker/autoscaler"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/docker/machine"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/instance"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/kubernetes"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/parallels"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/shell"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/ssh"
	_ "openbeagle.org/jkridner/gitlab-runner/executors/virtualbox"
	_ "openbeagle.org/jkridner/gitlab-runner/helpers/secrets/resolvers/azure_key_vault"
	_ "openbeagle.org/jkridner/gitlab-runner/helpers/secrets/resolvers/gcp_secret_manager"
	_ "openbeagle.org/jkridner/gitlab-runner/helpers/secrets/resolvers/vault"
	_ "openbeagle.org/jkridner/gitlab-runner/shells"
)

func init() {
	_, _ = maxprocs.Set()
	memlimit.SetGoMemLimitWithEnv()
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			// log panics forces exit
			if _, ok := r.(*logrus.Entry); ok {
				os.Exit(1)
			}
			panic(r)
		}
	}()

	fips.Check()

	app := cli.NewApp()
	app.Name = filepath.Base(os.Args[0])
	app.Usage = "a GitLab Runner"
	app.Version = common.AppVersion.ShortLine()
	cli.VersionPrinter = common.AppVersion.Printer
	app.Authors = []cli.Author{
		{
			Name:  "GitLab Inc.",
			Email: "support@gitlab.com",
		},
	}
	app.Commands = common.GetCommands()
	app.CommandNotFound = func(context *cli.Context, command string) {
		logrus.Fatalln("Command", command, "not found.")
	}

	cli_helpers.InitCli()
	cli_helpers.LogRuntimePlatform(app)
	cli_helpers.SetupCPUProfile(app)
	cli_helpers.FixHOME(app)
	cli_helpers.WarnOnBool(os.Args)

	log.ConfigureLogging(app)

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}
